# PTI - [Markdown basics](./instrukcja.md)

It is a homework solution from the PTI subject.

## Requirements
- Node.js
- Yarn

## Getting started

### Setup dependencies
```
$ yarn
```

### Generate table of contents for `instruction.md`
```
$ yarn run genToc
```
