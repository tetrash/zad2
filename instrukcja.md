# Lekcja 1 – Markdown lekki język znaczników

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
Spis treści

- [Wstęp](#wst%C4%99p)
- [Podstawy składni](#podstawy-sk%C5%82adni)
  - [Definiowanie nagłówków](#definiowanie-nag%C5%82%C3%B3wk%C3%B3w)
  - [Definiowanie list](#definiowanie-list)
  - [Wyróżnianie tekstu](#wyr%C3%B3%C5%BCnianie-tekstu)
  - [Tabele](#tabele)
  - [Odnośniki do zasobów](#odno%C5%9Bniki-do-zasob%C3%B3w)
  - [Obrazki](#obrazki)
  - [Kod źródłowy dla różnych języków programowania](#kod-%C5%BAr%C3%B3d%C5%82owy-dla-r%C3%B3%C5%BCnych-j%C4%99zyk%C3%B3w-programowania)
  - [Tworzenie spisu treści na podstawie nagłówków](#tworzenie-spisu-tre%C5%9Bci-na-podstawie-nag%C5%82%C3%B3wk%C3%B3w)
- [Edytory dedykowane](#edytory-dedykowane)
- [Pandoc – system do konwersji dokumentów Markdown do innych formatów](#pandoc--system-do-konwersji-dokument%C3%B3w-markdown-do-innych-format%C3%B3w)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Wstęp
Obecnie powszechnie wykorzystuje się języki ze znacznikami do opisania dodatkowych informacji
umieszczanych w plikach tekstowych. Z pośród najbardziej popularnych można wspomnieć o:
1. **html** – służącym do opisu struktury informacji zawartych na stronach internetowych,
2. **Tex** (Latex) – poznany na zajęciach język do „profesjonalnego” składania tekstów,
3. **XML** (Extensible Markup Language) - uniwersalnym języku znaczników przeznaczonym
   do reprezentowania różnych danych w ustrukturalizowany sposób. 
   
Przykład kodu html i jego interpretacja w przeglądarce:
```html
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Przykład</title>
</head>
<body>
<p> Jakiś paragraf tekstu</p>
</body>
</html>
```

Przykład kodu Latex i wygenerowanego pliku w formacie pdf
```TeX
\documentclass[]{letter}
\usepackage{lipsum}
\usepackage{polyglossia}
\setmainlanguage{polish}
\begin{document}
\begin{letter}{Szanowny Panie XY}
\address{Adres do korespondencji}
\opening{}
\lipsum[2]
\signature{Nadawca}
\closing{Pozdrawiam}
\end{letter}
\end{document}
```
Przykład kodu XML – fragment dokumentu SVG (Scalar Vector Graphics)
```html
<!DOCTYPE html>
<html>
<body>
<svg height="100" width="100">
 <circle cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="red" />
</svg>
 </body>
</html>
```
W tym przypadku mamy np. znacznik np. <circle> opisujący parametry koła i który może być
właściwie zinterpretowany przez dedykowaną aplikację (np. przeglądarki www).
Jako ciekawostkę można podać fakt, że również pakiet MS Office wykorzystuje format XML do
przechowywania informacji o dodatkowych parametrach formatowania danych. Na przykład pliki z
rozszerzeniem docx, to nic innego jak spakowane algorytmem zip katalogi z plikami xml.

```shell
$ unzip -l test.docx

Archive: test.docx
 Length Date Time Name
--------- ---------- ----- ----
 573 2020-10-11 18:20 _rels/.rels
 731 2020-10-11 18:20 docProps/core.xml
 508 2020-10-11 18:20 docProps/app.xml
 531 2020-10-11 18:20 word/_rels/document.xml.rels
 1421 2020-10-11 18:20 word/document.xml
 2429 2020-10-11 18:20 word/styles.xml
 853 2020-10-11 18:20 word/fontTable.xml
 241 2020-10-11 18:20 word/settings.xml
 1374 2020-10-11 18:20 [Content_Types].xml
```

Wszystkie te języki znaczników cechują się rozbudowaną i złożoną składnią i dlatego do ich edycji
wymagają najczęściej dedykowanych narzędzi w postaci specjalizowanych edytorów. By
wyeliminować powyższą niedogodność powstał **Markdown** - uproszczony język znaczników
służący do formatowania dokumentów tekstowych (bez konieczności używania specjalizowanych
narzędzi). Dokumenty w tym formacie można bardzo łatwo konwertować do wielu innych
formatów: np. html, pdf, ps (postscript), epub, xml i wiele innych. Format ten jest powszechnie
używany do tworzenia plików README.md (w projektach open source) i powszechnie
obsługiwany przez serwery git’a. Język ten został stworzony w 2004 r. a jego twórcami byli John
Gruber i Aaron Swartz. W kolejnych latach podjęto prace w celu stworzenia standardu rozwiązania
i tak w 2016 r. opublikowano dokument RFC 7764 który zawiera opis kilku odmian tegoż języka:
• CommonMark,
• GitHub Flavored Markdown (GFM),
• Markdown Extra.

## Podstawy składni
Podany link: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet zawiera opis
podstawowych elementów składni w języku angielskim. Poniżej zostanie przedstawiony ich krótki
opis w języku polskim.

### Definiowanie nagłówków

W tym celu używamy znaku kratki

Lewe okno zawiera kod źródłowy – prawe -podgląd przetworzonego tekstu

![headers](./images/Screenshot%202021-03-14%20at%2020.13.12.png)

### Definiowanie list
![lists](./images/Screenshot%202021-03-14%20at%2020.15.57.png)

Listy numerowane definiujemy wstawiając numery kolejnych pozycji zakończone kropką.

Listy nienumerowane definiujemy znakami: *,+,-

### Wyróżnianie tekstu

![text formatting](./images/Screenshot%202021-03-14%20at%2020.17.00.png)


### Tabele

![tables](./images/Screenshot%202021-03-14%20at%2020.18.31.png)

Centrowanie zawartości kolumn realizowane jest poprzez odpowiednie użycie znaku dwukropka:

### Odnośniki do zasobów

\[odnośnik do zasobów](www.gazeta.pl)

\[odnośnik do pliku](LICENSE.md)

\[odnośnik do kolejnego zasobu]\[1]

\[1]: http://google,com

### Obrazki

\!\[alt text](https://server.com/images/icon48.png "Logo 1") – obrazek z zasobów
internetowych

\!\[](logo.png) – obraz z lokalnych zasobów

### Kod źródłowy dla różnych języków programowania 

![programming language syntax](./images/Screenshot%202021-03-14%20at%2020.21.42.png)

### Tworzenie spisu treści na podstawie nagłówków
![table of contents](./images/Screenshot%202021-03-14%20at%2020.23.07.png)

## Edytory dedykowane

Pracę nad dokumentami w formacie Markdown( rozszerzenie md) można wykonywać w
dowolnym edytorze tekstowym. Aczkolwiek istnieje wiele dedykowanych narzędzi

1. Edytor Typora - https://typora.io/
1. Visual Studio Code z wtyczką „markdown preview”

![md editors](./images/Screenshot%202021-03-14%20at%2020.30.56.png)

## Pandoc – system do konwersji dokumentów Markdown do innych formatów

Jest oprogramowanie typu open source służące do konwertowania dokumentów
pomiędzy różnymi formatami.

Pod poniższym linkiem można obejrzeć przykłady użycia:

https://pandoc.org/demos.html

Oprogramowanie to można pobrać z spod adresu: https://pandoc.org/installing.html

Jeżeli chcemy konwertować do formatu latex i pdf trzeba doinstalować oprogramowanie
składu Latex (np. Na windows najlepiej sprawdzi się Miktex https://miktex.org/)

Gdyby podczas konwersji do formatu pdf pojawił się komunikat o niemożliwości
znalezienia programu pdflatex rozwiązaniem jest wskazanie w zmiennej środowiskowej 
PATH miejsca jego położenia

![setting path env 1](./images/Screenshot%202021-03-14%20at%2020.33.35.png)

![setting path env 2](./images/Screenshot%202021-03-14%20at%2020.33.59.png)

Pod adresem (https://gitlab.com/mniewins66/templatemn.git) znajduje się przykładowy plik
Markdown z którego można wygenerować prezentację w formacie pdf wykorzystując
klasę latexa beamer.

W tym celu należy wydać polecenie z poziomu terminala:

$pandoc templateMN.md -t beamer -o prezentacja.pdf
